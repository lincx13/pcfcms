## 安装
> 运行环境要求PHP7.1+。
## 一键安装
上传你的代码，站点入口目录设置/public
在浏览器中输入你的域名或IP（例如：www.pcfcms.com）,
安装程序会自动执行安装。期间系统会提醒你输入数据库信息以完成安装，安装完成后建议删除install目录或将其改名。

后台访问地址：
1.域名/login.php
提示：正常访问是第一中模式，第一种访问不了请检测[URL重写](http://help.pcfcms.com)是否配置好
安装过程中请牢记您的账号密码！

## 重新安装
1. 清除数据库
2. 删除public/install/install.lock 文件
3. 删除config/install.lock 文件

## 手动安装
1.创建数据库，倒入数据库文件
数据库文件目录/public/install/pcfcms.sql

2.修改数据库连接文件
配置文件路径/config/database.php
~~~
// 数据库类型
'type'              => 'mysql',
// 服务器地址
'hostname'          => '127.0.0.1',
// 数据库名
'database'          => 'pcfcms',
// 用户名
'username'          => 'root',
// 密码
'password'          => 'root',
// 端口
'hostport'          => '3306',
// 数据库连接参数
'params'            => [],
// 数据库编码默认采用utf8
'charset'           => 'utf8',
// 数据库表前缀
'prefix'            => 'pcf_',
~~~

3.修改目录权限（linux系统）777
/public
/runtime

4.后台登录：
http://www.yourname.com/login.php
默认账号：pcfcms 密码：pcfcms.com