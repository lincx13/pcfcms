<?php
/***********************************************************
 * 节点管理
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\system;
use app\admin\controller\Base;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
use think\facade\Cache;

class Menu extends Base
{
    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
    }

    public function index()
    {
        //验证查看权限
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        return $this->fetch();
    }

    public function get_menu()
    {
        $list = Cache::get('getmenu');
        if(!$list){
            $list = Db::name('menu')->order('sort asc')->select()->toArray();
            foreach ($list as $key => $value) {
                if($value['type'] == 3 || $value['type'] == 4){
                   $list[$key]['open'] = false;
                }else{
                   $list[$key]['open'] = true; 
                }
            }
            Cache::set('getmenu',$list,PCFCMS_CACHE_TIME);//节点缓存
        }
        if($list){
            $result = ['code' => 0, 'msg' => 'ok','data' => $list];
            return json($result);            
        }else{
            $result = ['code' => 1, 'msg' => 'no','data' =>''];
            return json($result);       
        }
    }

    public function add()
    {
        if (Request::isPost()) { 
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            } 
            $post = input('param.');
            $add_data = array();
            if (!$post['name']){
                $result = ['code' => 1, 'msg' => '请输入权限名称'];
                return json($result);
            }
            if (!$post['type']){
                $result = ['code' => 1, 'msg' => '请选择权限类型'];
                return json($result);
            }
            $add_data['parent_id'] = isset($post['parentId']) && !empty($post['parentId']) ? $post['parentId']:'';//上级菜单id
            $add_data['name'] = $post['name'];//权限名称
            $add_data['icon'] = $post['icon'];//图标
            $add_data['url'] = $post['url'];//URL地址
            $add_data['type'] = $post['type'];//类型
            $add_data['auth'] = $post['auth'];//权限标识
            $add_data['sort'] = isset($post['sort']) && !empty($post['sort']) ? $post['sort'] : '10';//显示顺序 
            $add_data['param'] = $post['param'];//参数
            $add_data['is_show'] = $post['is_show1'];//状态
            $add_data['create_time'] = time(); //时间 
            if (Db::name('menu')->save($add_data)) {
                Cache::clear('getmenu');
                $result = ['code' => 0, 'msg' => '添加成功'];
                return json($result);
            } else {
                $result = ['code' => 1, 'msg' => '添加失败'];
                return json($result);
            }
        }
    }

    public function edit()
    {
        if (Request::isPost()) { 
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            } 
            $post = input('param.');
            $add_data = array();
            if (!$post['name']){
                $result = ['code' => 1, 'msg' => '请输入权限名称'];
                return json($result);
            }
            if (!$post['type']){
                $result = ['code' => 1, 'msg' => '请选择权限类型'];
                return json($result);
            }
            $add_data['id'] = $post['id'];
            $add_data['parent_id'] = isset($post['parentId']) && !empty($post['parentId']) ? $post['parentId']:'';//上级菜单id
            $add_data['name'] = $post['name'];//权限名称
            $add_data['icon'] = $post['icon'];//图标
            $add_data['url'] = $post['url'];//URL地址
            $add_data['type'] = $post['type'];//类型
            $add_data['auth'] = $post['auth'];//权限标识
            $add_data['sort'] = isset($post['sort']) && !empty($post['sort']) ? $post['sort']:'10';//显示顺序 
            $add_data['param'] = $post['param'];//参数
            if(!empty($post['is_show']) || $post['is_show'] != ''){
                $add_data['is_show'] = $post['is_show'];//状态
            }
            $add_data['update_time'] = time(); //时间 
            if (Db::name('menu')->save($add_data)) {
                Cache::clear('getmenu');
                $result = ['code' => 0, 'msg' => '修改成功'];
                return json($result);
            } else {
                $result = ['code' => 1, 'msg' => '修改失败'];
                return json($result);
            }
        }
    }

    public function pcfdel()
    {
        if (Request::isPost()) {
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            }
            $post = input('param.');
            $id = $post['id'];
            if(Db::name('menu')->where('id',$id)->delete()){
                Db::name('menu')->where('parent_id',$id)->delete();
                Cache::clear('getmenu'); // 清空节点缓存
                $result = ['code' => 0, 'msg' => '删除成功'];
                return $result;                
            }else{
                $result = ['code' => 1, 'msg' => '删除失败'];
                return $result;    
            }
        } 
    }

}