<?php
/***********************************************************
 * 模型字段模型
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use think\Model;
class Channelfield extends Model
{
    //字段列表
    public function tableData($post,$channel_id){
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $condition = array();
        //显示主表与附加表
        $condition['channel_id'] = $channel_id;
        $condition['ifcontrol'] = 0;//为了不显示系统内置字段
        $list = Db::name('channelfield')->field('*')->where($condition)
            ->order('sort_order asc, ifmain asc, ifcontrol asc, id desc')
            ->paginate($limit);
        $data = $this->tableFormat($list->getCollection())->toArray();
        foreach ($data as $key => $value) {
            $data[$key]['pcfchannel_id'] = $channel_id;
            $data[$key]['dtypename'] = Db::name('field_type')->where('name', $value['dtype'])->value('title');
            $data[$key]['add_time'] = pcftime($value['add_time']);
            $data[$key]['update_time'] = pcftime($value['update_time']);
        }
        $re['code'] = 0;
        $re['msg'] = '';
        $re['count'] = $list->total();
        $re['data'] = $data;
        return $re;
    }

    protected function pcftableWhere($post){
        $result['field'] = "*";
        $result['order'] = [];
        return $result;
    }

    protected function tableFormat($list)
    {
        return $list;
    }

}
