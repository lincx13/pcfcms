<?php

namespace app\home\model;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Session;
class Users
{
    protected function initialize()
    {
        parent::initialize();
    }

    // 获取当前会员信息
    public function getinfo(){
        $users_id = Session::get('pcfcms_users_id');
        $users = Db::name('users')->field('b.*, a.*')
            ->alias('a')
            ->join('users_level b', 'a.level_id = b.id', 'LEFT')
            ->where('a.id',$users_id)->find();
        unset($users['password']);
        return $users;
    }

}