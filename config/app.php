<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用地址
    'app_host'         => '',
    // 应用的命名空间
    'app_namespace'    => '',
    
    // 是否启用路由
    'with_route'       => true,
    // 是否启用事件
    'with_event'       => true,
    // 自动多应用模式
    'auto_multi_app'   => flase,

    // 默认应用
    'default_app'    => 'home',

    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => ['*'=>'', 'api'=>'api', 'admin'=>'admin','home'=>'home'],

    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [],
    
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => ['common','extra'],

    // 异常页面的模板文件
    //'exception_tmpl'   => base_path().'admin/view/public/message.html',
    
    'http_exception_template' => array(
        //403 => public_path().'public/common/errpage/403.html',
        //404 => public_path().'public/common/errpage/404.html',
        //500 => public_path().'public/common/errpage/500.html',
    ),
    // 错误显示信息,非调试模式有效
    'error_message'    => '请联系技术处理：QQ 1131680521',
    
    // 显示错误信息
    'show_error_msg'   => true,

];